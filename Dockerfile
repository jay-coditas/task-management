# FROM node:12.14.0

# WORKDIR /app

# COPY package*.json ./

# RUN npm install

# RUN npm install -g pm2

# COPY . .

# EXPOSE 3000

# CMD npm run start


FROM node:10-alpine
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY . .
RUN npm run build
CMD ["npm", "run", "start"]